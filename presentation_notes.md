# demogrep - an example command-line tool

## table of contents
- [why the command-line](#why-the-command-line)?
    - simple. portable.
    - composable.
    - `demogrep log < demogrep.go | bat --language go`

- [anatomy of a command-line](#anatomy-of-the-command-line)
    - stdin, stdout, stderr
    - exit codes
- [command-line aplpications in go](#what-does-this-look-like-in-go)
    - as a function signature
        - [don't use fmt.Print!](#stderr-and-you)
        - don't write interfaces!
    - testing a command-line application
    - know your audience
    - Volvo, Ikea, and good design.
- putting it together: `demogrep`[#putting-it-together]
    - main()
    - Demogrep()
    - Opts
- odds and ends
    - [tldr](https://github.com/dbrgn/tealdeer)
    - [ripgrep](https://github.com/BurntSushi/ripgrep)
    - [fd](https://github.com/sharkdp/fd)
## why the command-line?
- the command line is portable.
- command-line is a great match for go: portable, simple.
- GUIs are hard to send input to and from:
    - everything is a great program for finding files, but what if I want to process the files? what if I want a list of every rust file in my home directory, sorted by size? I could definitely do it with everything.
- command-line applications can be composed!

![everything](images/everything.PNG)
    
`fd --extension rs`

## anatomy of the command-line.
a classic command-line program has five parts.
- **READS** an input stream of bytes
- **WRITES** an output stream of bytes
- **WRITES** diagnostic output as a stream of bytes.
- **EXITS** with an exit code. 0 means failure, 1+ means error.
- **CONFIGURED** by one or more arguments or flags.

## what does this look like in go?

```go
import "io"
import "os"
func main() {
    options := parseOptions()
    err := AnyCommandLineProgram(options, io.Stdin, io.Stdout, io.Stderr)
    if err != nil {
        fmt.Fprintln(io.Stderr, err)
        os.Exit(1)
    }
    // this happens automatically when main() returns, but we're including it here for scompleteness.
    os.Exit(0) 

}

func AnyCommandLineProgram(options struct{}, r io.Reader, w io.Writer, e io.Writer) error {
       // the magic happens here.
}
```


## stderr and you
don't use fmt.Print for diagnostic info. use fmt.Fprint(os.Stderr), or log.Print, which writes to stderr by default. this lets you cleanly separate the OUTPUT of a program and info ABOUT it.

## exit codes
these are mostly a relic. 99% of the time, you only need two exit codes: 1 (failure) and 0 (ok).

## why not just use Stdin, Stdout, and Stderr directly?

- it's much harder to test; we have to set up pipes and redirection and stuff like that.
- using these minimal interfaces, we can make tests that just take strings as input!
- other people can use the logic of our program within their own without shelling out.

```go
func TestAnyCommandLineProgram(t *testing.T) {
    type test = struct {
        options struct{}
        input string
        wantStdoutContains, wantStderrContains, wantExitErrorContains string
    }
    for name, tt := range map[string]test {
        "hello, world": {wantStdoutContains: "hello, world"},
    } {
        stdin := strings.NewReader(input)
        stdout, stderr := new(bytes.Buffer), new(bytes.Buffer)
        err := AnyCommandLineProgram(tt.options, stdin, stdout, stderr)
        if wantExitErrorContains != "" && err == nil || err != nil && !strings.Contains(err.Error(), wantExitErrorContains) {
            t.Fatalf("expected an exit error containing %s, but got %v",wantExitErrorContains, err)
        }
        if !strings.Contains(stdout.String(), tt.wantStdoutContains) {
            t.Fatalf("expected stdout to contain %s, but got %s", tt.wantStdoutContains, stdout)
        }
        if !strings.Contains(stderr.String(), tt.wantStderrContains) {
            t.Fatalf("expected stderr to contain %s, but got %s")
        }
    }

}
```

## so I should write interfaces, right? NO.

a command-line application shouldn't be complicated enough to need to write interfaces. function types are normally fine. if your program is getting complicated enough to need interfaces, maybe it's no longer a nice CLI application and more of a library that a CLI application should use.


## know your audience / principles of engineering

- the user shouldn't have to know go
- the user should not be able to silently fail
- error messages should guide the user to the correct path.
- if the program boots, it should be configured correctly.

## the user shouldn't have to know go:
    - example: IKEA
    - example: VOLVO
## the user shouldn't be able to silently fail:
    - example: IKEA (bad)

## error messages should guide the user to the correct path
```
$ demogrep
ERROR 2020/08/21 08:07:42 demogrep.go:18: parsing command-line options: expected an argument PATTERN. Try demogrep --help for more information on command-line flags, or read README.md for examples and documentation.
```
```
$ demogrep --help
Usage of demogrep:
  -A int
        how many lines of context [A]fter the match to print
  -B int
        how many lines of context [B]efore the match to print
  -C int
        how many lines of [C]ontext around the match to print
  -e    match using string lit[e]rals instead of regular expressions
  -i    [i]gnore case in matches
  -n    whether to print the line [n]umbers
  -posix
        use [posix] regular expresisons
  -v    [v]erbose debug info
```
```
$ echo $'foo\nbar\nbaz' | demogrep foo -A 1
ERROR 2020/08/21 08:08:12 demogrep.go:18: parsing command-line options: demogrep does not (yet) support more than one positional argument. If you set flags, they need to go before the positional arguments, not after. Try demogrep --help for more information on command-line flags, or read README.md for examples and documentation.
```
```
# echo 'foo\nbar\nbaz'| demogrep -A 1 foo
foo
bar
```


## enforce correct configuration
- the best way to do this is not by using the flags directly. instead, parse the flags into a struct which represents a happy path for program execution.

- eg, these flags in [opts.go](https://gitlab.com/efronlicht/demogrep-go/-/blob/master/opts/opts.go#L14)
```go
// raw CLI flags, only used to create an Opts during Parse().
// note that they're not exported and demogrep.go can't see them at all!
var (
	after       = flag.Int("A", 0, "how many lines of context [A]fter the match to print")
	before      = flag.Int("B", 0, "how many lines of context [B]efore the match to print")
	context     = flag.Int("C", 0, "how many lines of [C]ontext around the match to print")
	ignoreCase  = flag.Bool("i", false, "[i]gnore case in matches")
	lineNumbers = flag.Bool("n", false, "whether to print the line [n]umbers")
	literal     = flag.Bool("e", false, "match using string lit[e]rals instead of regular expressions")
	posix       = flag.Bool("posix", false, "use [posix] regular expresisons")
	verbose     = flag.Bool("v", false, "[v]erbose debug info")
)
```
are parsed into the [Opts struct](https://gitlab.com/efronlicht/demogrep-go/-/blob/master/opts/opts.go#L27)
```go
// Opts represent the parsed and validated options. Use these instead of the command-line flags directly.
// You need to set the Matches func somehow; New() is a good way to do it.
type Opts struct {
    // Lines of context to match. Default is 0, 0.
    // -A, -B, and -C influence this
    Context struct{ Before, After int }
    // Whether to print line numbers. Default is 0.
    // -n influences thiss

    LineNumbers bool
    // A function to match a line of text.
    // --posix, -i, and -e influence this
    Matches func(string) bool
    // Print verbose debug output.
    // -v influences this
    Verbose bool
}
```


gi## putting it together: [demogrep](https://gitlab.com/efronlicht/demogrep-go)

- [demogrep.go](demogrep.go)/main(demogrep/demogre)main.go()
- demogrep.go/Demogrep()
- demogrep.go/opts.go/Opts
- demogrep.go/demogrep_test.go
## odds and ends

## TLDR

I forgot how to use `curl`. Let's look at the manual...

```
man curl
NAME
       curl - transfer a URL

SYNOPSIS
       curl [options] [URL...]

DESCRIPTION
       curl  is a tool to transfer data from or to a server, using one of the supported protocols (DICT, FILE, FTP, FTPS, GOPHER,
       HTTP, HTTPS, IMAP, IMAPS, LDAP, LDAPS, POP3, POP3S, RTMP, RTSP, SCP, SFTP, SMB, SMBS, SMTP, SMTPS, TELNET and  TFTP).  The
       command is designed to work without user interaction.
    # and so on, for another 2800 lines.
```

can't I just get the basics?

$ tldr curl

# curl

> Transfers data from or to a server.
> Supports most protocols, including HTTP, FTP, and POP3.
> More information: <https://curl.haxx.se>.

- Download the contents of an URL to a file:

`curl {{http://example.com}} -o {{filename}}`

- Download a file, saving the output under the filename indicated by the URL:

`curl -O {{http://example.com/filename}}`

- Download a file, following [L]ocation redirects, and automatically [C]ontinuing (resuming) a previous file transfer:

`curl -O -L -C - {{http://example.com/filename}}`

- Send form-encoded data (POST request of type `application/x-www-form-urlencoded`). Use `-d @file_name` or `-d @'-'` to read from STDIN:

`curl -d {{'name=bob'}} {{http://example.com/form}}`

- Send a request with an extra header, using a custom HTTP method:

`curl -H {{'X-My-Header: 123'}} -X {{PUT}} {{http://example.com}}`

- Send data in JSON format, specifying the appropriate content-type header:

`curl -d {{'{"name":"bob"}'}} -H {{'Content-Type: application/json'}} {{http://example.com/users/1234}}`

- Pass a user name and password for server authentication:

`curl -u myusername:mypassword {{http://example.com}}`

- Pass client certificate and key for a resource, skipping certificate validation:

`curl --cert {{client.pem}} --key {{key.pem}} --insecure {{https://example.com}}`

