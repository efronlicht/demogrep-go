# demogrep
A fun `grep` clone for demonstration purposes!

Demogrep searches `stdin` for lines of text matching a pattern and outputs them to stdout.



## installation

    go install demogrep.go

## usage

    demogrep [OPTIONS] PATTERN

## examples 

###  get help
        
    demogrep --help

###  filter the output of a program's stderr stream by appending the lines that contain ERROR to a file

    myprogram 2> demogrep ERROR >> myprogram_errors.log

### search for the word "log" in demogrep.go, case-insensitive
    
     demogrep -i log < demogrep.go 


