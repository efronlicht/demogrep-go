package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"local/demogrep/logging"
	"local/demogrep/opts"
	"os"
	"strings"
)

func main() {
	o, err := opts.Parse()
	log := logging.Error(os.Stderr)
	if err != nil {
		log.Fatalf("parsing command-line options: %v. Try demogrep --help for more information on command-line flags, or read README.md for examples and documentation.", err)
	}
	err = Demogrep(o, os.Stdin, os.Stdout, os.Stderr)

	if err != nil {
		log.Fatalf("running demogrep: %v", err)
	}
	os.Exit(0)
}

// Demogrep runs the program with the specified options. When main() is called, stdin, stdout, and stderr are what you'd expect them to be.
func Demogrep(o opts.Opts, stdin io.Reader, stdout, stderr io.Writer) error {
	// this will choke on large files and might waste a lot of memory.
	// ideally, we'd use a buffer of some kind and only read in enough to handle our lines of context before and after.
	b, err := ioutil.ReadAll(stdin)
	if err != nil {
		return err
	}
	logger := logging.Debug(stderr, o.Verbose)
	lines := strings.Split(string(b), "\n")

	toWrite := make([]bool, len(lines))

	for i, line := range lines {
		if o.Matches(line) {
			logger.Printf("match: line %d: %s", i, line)
			start := max(0, i-o.Context.Before)
			end := min(len(lines), i+o.Context.After+1)
			for i := start; i < end; i++ {
				toWrite[i] = true

			}
		}
	}
	// go doesn't buffer stdin, out, or err by default.
	// we're about to do a bunch of repeated writes very quickly, so it's quicker to buffer them.
	// remember to call Flush() when we're done, or some of the writes might never make it to the underlying io.Writer (in most cases, the OS).
	out := bufio.NewWriter(stdout)
	if o.LineNumbers {
		for i, line := range lines {
			if toWrite[i] {
				fmt.Fprintf(out, "%d\t%s\n", i, line)
			}
		}
	} else {
		for i, line := range lines {
			if toWrite[i] {
				fmt.Fprintln(out, line)
			}
		}
	}
	return out.Flush()
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
